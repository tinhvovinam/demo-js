// const $ = document.querySelector.bind(document);
// const $$ = document.querySelectorAll.bind(document);
// note : trong function bth có context , arrow function ko có context đồng nghĩa từ khóa this sử dụng k đc .
// acc : là giá trị hiện tại được vòng lặp trước trả về 
// item : là từng giá trị ( phần tử) trong cái mảng 
class Product {

    cartItems = [];

    constructor(productList) {
        this._produclist = productList;
    }

    get list() {
        return this._produclist;
    }

    set list(newList) {
        this._produclist = newList;
    }

    BASE_URL = " https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";
    loadProduct = (dataType) => {
        axios({
            url: this.BASE_URL,
            method: "GET",
        }).then((res) => {
            this.renderProductHTML(res.data, dataType);
        });
    }
    renderProductHTML = (data, dataType) => {
        // [1, 2, 3, 4].filter(item => item > 3); //[4] 
        const dt = dataType === "all" ? data : data.filter((item) => item.type.toLowerCase() == dataType);
        const contentHTML = dt.reduce((acc, item, index, data) => {
            return acc += `<div id="${item.id}" class="product-item product-item-col">
            <img src="${item.img}" alt="">
            <p>${item.desc}</p>
            <button>Cart</button>
            </div>`;
        }, {
            a: "ABC",
            b: "123465",
        });
        const productList = document.querySelector("#productList");
        console.log(productList, 'a')
        productList.innerHTML = contentHTML;
        setTimeout(() => {
            // const elements = [...productList.querySelectorAll(div.product-item)];
            // console.log(elements, 'el');
            dt.forEach((item) => {
                const element = productList.querySelector(`[id ='${item.id}']`); //64
                const button = element.querySelector('button');
                button.onclick = () => {
                        this.addToCart(item)
                    }
                    // console.log(button);
            });
            // elements.forEach(element => {
            //     const button = element.querySelector('button');
            //     button.onclick = () => {
            //         this.addToCart(item)
            //     }
            //     console.log(button);
            // });
        })
    };
    addToCart = (item) => {
        this.cartItems.push(item);
        console.log("Cartitems", this.cartItems);
    }

    getDataType = () => {
        const dataType = document.querySelector('#filter').value;
        if (dataType === "samsung") {
            this.loadProduct(dataType)
        } else if (dataType === "iphone") {
            this.loadProduct(dataType)
        } else {
            this.loadProduct(dataType)
        }
    }
    renderCart = (itemCarts) => {
        let dtCart = itemCarts

    }
};

const product = new Product([]);
// const loadProduct = product.loadProduct
const { BASE_URL, renderProductHTML, loadProduct, addToCart, getDataType } = product;
loadProduct('all');