const Base_URL = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";
let idSanPhamThemGioHang = null;


let gioHang = []; //cart[]




// lấy danh sách & hiển thị sản phẩm 
// var hienThiDanhSach = function () {
//     axios({
//         url: Base_URL,
//         method: "GET",
//     })
//         .then(function (res) {
//             console.log("Data API", res.data);
//             renderTableSanPham(res.data);
//         })
//         .catch(function (err) {
//             console.log("no", err);
//         });
// }

let hienThiDanhSach = () => {
    axios({
            url: Base_URL,
            method: "GET",
        })
        .then(function(res) {
            console.log("Data API", res.data);
            renderTableSanPham(res.data);
        })
        .catch(function(err) {
            console.log("no", err);
        });
}


// render danh sách sản phẩm
function renderTableSanPham(data) {
    var contentHTML = "";
    for (var i = 0; i < data.length; i++) {
        var sanPham = data[i];
        var contentItem = `
                        <div class="product-item">
                            <div class="product-content">
                                <div class="product-image">
                                    <img src="${sanPham.img}" alt="" />
                                </div>
                            <div class="product-info">
                                <h4 class="product-name">${sanPham.name}</h4>
                                <h4 class="product-price">${sanPham.price}</h4>
                            </div>
                            <div class="btn">
                                <button type="button" onclick=themSanPhamVaoGioHang(${sanPham.id})>Add to Cart</button>
                            </div>
                            </div>
                        </div>
                        `;
        contentHTML += contentItem;
    }
    document.getElementById("product-list").innerHTML = contentHTML;
    // console.log("contentHTML", contentHTML);
}

// Sự kiện thay đổi select
document.getElementById("brand").addEventListener("change", function() {
    let resultChange = document.getElementById("brand").value;
    if (resultChange === "all") {
        hienThiDanhSach();
    } else {
        locSanPhamTheoHang(resultChange);
    }

});

// Lọc sản phẩm theo hãng
function locSanPhamTheoHang(brandName) {
    let listProductByBrand = [];

    axios({
            url: Base_URL,
            method: "GET",
        })
        .then(function(res) {
            let listProducts = res.data;
            console.log(listProducts);
            // lọc mảng sản phẩm
            for (var i = 0; i < listProducts.length; i++) {
                if (listProducts[i].type.toLowerCase() === brandName.toLowerCase()) {
                    listProductByBrand.push(listProducts[i]);
                }
            }
            renderTableSanPham(listProductByBrand);
        })
        .catch(function(err) {
            console.log("no", err);
        });
}

// Thêm sản phẩm vào giỏ hàng
function themSanPhamVaoGioHang(id) {
    let sanPhamThem = {};
    let flag = false;
    axios({
            url: `${Base_URL}/${id}`,
            method: "GET",
        })
        .then(function(res) {
            for (let i = 0; i < gioHang.length; i++) {
                let sanPhamTrongGio = gioHang[i];
                // Kiểm tra đã có trong giỏ hàng
                if (+sanPhamTrongGio.sanpham.id === id) {
                    gioHang[i].soluong += 1;
                    flag = true;
                }
            }
            if (!flag) {
                sanPhamThem.sanpham = res.data;
                sanPhamThem.soluong = 1;
                gioHang.push(sanPhamThem);
            }

            renderCart(gioHang);
            tongTienThanhToan();
            saveCartInLocal();
        })
        .catch(function(err) {
            console.log("no", err);
        });
}

// Render giỏ hàng
function renderCart(data) {
    var contentHTML = "";
    for (var i = 0; i < data.length; i++) {
        var spGioHang = data[i];
        var trContent = ` <tr>
                                <td class="td-img">
                                    <img src="${spGioHang.sanpham.img}" alt"" />  
                                </td>
                                <td> ${spGioHang.sanpham.name} </td>
                                <td> ${spGioHang.sanpham.price} </td>
                                <td> 
                                    <span>${spGioHang.soluong} </span>
                                    <button onclick=themSoLuong(${spGioHang.sanpham.id})><i class="fa-solid fa-angle-up"></i></button>
                                    <button onclick=giamSoLuong(${spGioHang.sanpham.id})><i class="fa-solid fa-angle-down"></i></button>
                                </td>
                                <td> ${spGioHang.sanpham.price * spGioHang.soluong} </td>
                                <td>
                                    <button onclick="deleteProduct(${spGioHang.sanpham.id})" > <i class="fa-solid fa-xmark"></i> </button>
                                </td>
                            </tr>
                        `;
        contentHTML += trContent;
    }
    document.getElementById("tblGioHang").innerHTML = contentHTML;
}

// Tăng số lượng sản phẩm trong giỏ hàng
function themSoLuong(id) {
    for (var i = 0; i < gioHang.length; i++) {
        if (+gioHang[i].sanpham.id === id) {
            gioHang[i].soluong += 1;
        }
    }
    renderCart(gioHang);
    tongTienThanhToan();
    saveCartInLocal();
}

// Tăng số lượng sản phẩm trong giỏ hàng
function giamSoLuong(id) {
    for (var i = 0; i < gioHang.length; i++) {
        if (+gioHang[i].sanpham.id === id) {
            if (gioHang[i].soluong === 1) {
                gioHang.splice(i, 1);
                renderCart(gioHang);
                tongTienThanhToan();
                saveCartInLocal();
                return;
            }
            gioHang[i].soluong -= 1;
        }
    }
    renderCart(gioHang);
    tongTienThanhToan();
    saveCartInLocal();
}


// Tổng tiền thanh toán giỏ hàng
function tongTienThanhToan() {
    let total = 0;
    let cost = 0;
    for (var i = 0; i < gioHang.length; i++) {
        cost = gioHang[i].sanpham.price * gioHang[i].soluong;
        total += cost;
    }
    document.getElementById("pay-money").innerHTML = `${total}đ`;
}



// Thanh toán
function thanhToan() {
    gioHang = [];
    saveCartInLocal();
    renderCart(gioHang);
    tongTienThanhToan();
}

// Lưu giỏ hàng xuống Local Storage
var saveCartInLocal = function() {
    // localStorage có 2 tham số: 1. key , 2. dữ liệu
    // chỉ nhận dữ liệu chuỗi, k lưu được mảng => chuyển sang JSON
    // cú pháp JSON.stringify()
    const jsonData = JSON.stringify(gioHang);
    console.log(jsonData);
    localStorage.setItem("carts", jsonData);
}

// Function: load dữ liệu cart từ local
let getDataCart = function() {
    // Kiểm tra key có tồn tại không
    var dataJson = localStorage.getItem("carts"); //carts là key
    if (dataJson) {
        // nếu dữ liệu k null thì parse ra data
        var data = JSON.parse(dataJson);
        // sau đó renew data lại để có các method trong Object
        for (var i = 0; i < data.length; i++) {
            var newProduct = new Product(
                data[i].sanpham,
                data[i].soluong
            );
            gioHang.push(newProduct);
        }
        renderCart(gioHang);
        console.log("Giỏ Hàng:", gioHang);
    }
}

// FUNCTION: DELETE SẢN PHẨM TRONG GIỎ HÀNG
var deleteProduct = function(id) {
    var foundIndex = findProductById(id);
    if (foundIndex === -1) {
        alert(" id không tồn tại");
        return;
    }
    gioHang.splice(foundIndex, 1);
    saveCartInLocal();
    renderCart(gioHang);
    tongTienThanhToan();
}

// FUNCTION: HÀM TRẢ VỀ VỊ TRÍ SẢN PHẨM TRONG MẢNG GIỎ HÀNG
var findProductById = function(id) {
    for (var i = 0; i < gioHang.length; i++) {
        if (+gioHang[i].sanpham.id === id) {
            return i;
        }
    }
    return -1;
}



//================== LOADING DEFAULT =======================
// Load dữ liệu mặc định từ DB khi vào trang web
hienThiDanhSach();
getDataCart();
tongTienThanhToan();